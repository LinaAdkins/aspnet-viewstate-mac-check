#
# Checks to see if the page viewstate is protected by MAC or is vulnerable to base64 decode.
# Author: Lina Adkins

include("compat.inc");

if(description){
 script_id(93893);
 script_name(english:"ASP.NET viewstate MAC check");
 script_version("$Revision: 1.00 $");
 script_cvs_date("$Date: 2015/04/13 16:33:06 $");
 script_cwe_id("CWE-642");

 script_set_attribute(attribute:"synopsis", value:
"State data stored in client pages is easily decrypted if viewstate MAC is not enabled for the page." );
 script_set_attribute(attribute:"description", value:
"A client connected to an unprotected page can decrypt the viewstate info and potentially tamper with viewstate data sent to the server." );
 script_set_attribute(attribute:"solution", value: "Update asp.net libraries or update to version 4.5.2 or greater.");
 script_set_attribute(attribute:"exploitability_ease", value:"Any base64 decoder will decode viewstate data.");
 script_set_attribute(attribute:"exploit_available", value:"true");
 script_summary(english:"Checks for unprotected viewstate");
 script_copyright(english:"Copyright 2015 Lina Adkins");
 script_category(ACT_GATHER_INFO);
 script_family(english:"Web Servers");
 script_dependencie("http_version.nasl");
 script_require_ports("Services/www", 80);

 exit(0);

}

port = get_http_port(default:80);

include("global_settings.inc");
include("misc_func.inc");
include("http.inc");

w = http_send_recv3(method:"GET", item:"/", port: port , follow_redirect:1);
if(isnull(w)){ exit(0); }

viewstate = egrep(pattern:"VIEWSTATE", string: w[2]);
if(viewstate==""){ exit(0); }

# Trim so that we only get our viewstate string
var match = eregmatch(string:viewstate, pattern:'value="(.+)"');
trimmed = match[0];
trimmed = trimmed - 'value="';
trimmed = trimmed - '"';

# PASS: Incorrect block size for base64
if( (strlen(trimmed) % 4) != 0){
	Pass();
}

# PASS: Charset does not seem correct for base64
if(!ereg(pattern:"^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$" , string: trimmed ))
{
	Pass();
}

# Our viewstate string could not be identified as non-base64, so we fail
Fail();

function Fail()
{
	#display("Failed!");
	security_warning(port);
	exit(0);
}

function Pass()
{
	#display("Passed!");
	exit(0);
}


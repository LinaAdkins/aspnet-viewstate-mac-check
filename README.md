# README #

This NASL script lets the Nessus security scanner check asp.net applications to see if they are encrypting the contents of their viewstate. It will pull down the contents of the page requested, then get the viewstate info and check to see if it fits a base64 encoding footprint.